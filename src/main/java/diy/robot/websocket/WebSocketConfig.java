package diy.robot.websocket;

import diy.robot.autonom.AutonomManager;
import diy.robot.autonom.CodeCompilerAndRunner;
import diy.robot.domain.KeyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    private KeyMap keyMap;
    @Autowired
    private AutonomManager autonomManager;
    @Autowired
    private CodeCompilerAndRunner codeCompilerAndRunner;


    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new SocketMessageHandler(keyMap,autonomManager,codeCompilerAndRunner), "/ws-robot");
    }
}
