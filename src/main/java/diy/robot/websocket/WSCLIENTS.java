package diy.robot.websocket;

import java.util.Arrays;

public enum WSCLIENTS {

    WEBCLIENT("webclient"),
    LASER("laser"),
    MOTORCONTROLWS("motorControllWS"),
    ULTRASONICFRONT("ultrasonicFront"),
    ULTRASONICBACK("ultrasonicBack"),
    LIGHTSENSOR("lightSensor"),
    RFIDSENSOR("rfidSensor"),
    ROBOTARM("robotArm"),
    BATTERYVOLTAGE("batteryVoltage"),
    UNKNOWN("unknown");

    String websocketClientName;

    WSCLIENTS(String websocketClientName) {
        this.websocketClientName = websocketClientName;
    }

    public static WSCLIENTS valueOfByWSClientName(String websocketClientName) {
        return Arrays.stream(WSCLIENTS.values())
                .filter((value) -> value.getWebsocketClientName().equals(websocketClientName)).findAny().orElse(UNKNOWN);
    }

    public String getWebsocketClientName() {
        return websocketClientName;
    }
}
