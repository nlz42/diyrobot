package diy.robot.websocket;


import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import diy.robot.autonom.AutonomManager;
import diy.robot.autonom.CodeCompilerAndRunner;
import diy.robot.domain.KeyMap;
import diy.robot.drivers.Watchdog;
import diy.robot.robot.RobotCurrentState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

@Component
@Slf4j
public class SocketMessageHandler extends TextWebSocketHandler {

    private static BiMap<WSCLIENTS, WebSocketSession> sessions = HashBiMap.create();

    private KeyMap keyMap;
    private AutonomManager autonomManager;
    private CodeCompilerAndRunner codeCompilerAndRunner;

    public SocketMessageHandler(KeyMap keyMap, AutonomManager autonomManager, CodeCompilerAndRunner codeCompilerAndRunner) {
        this.keyMap = keyMap;
        this.autonomManager = autonomManager;
        this.codeCompilerAndRunner = codeCompilerAndRunner;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {

        log.info("Connected ... " + session.getId());
    }


    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        log.error("### DISCONNECTED ####" + sessions.inverse().get(session));
        WSCLIENTS wsclient = sessions.inverse().get(session);
        sessions.remove(wsclient);
        Watchdog.restartDisconnectClient(wsclient);
    }

    public static void sendTo(WSCLIENTS reciever, String msg) {

        try {
            if (sessions.get(reciever) != null) {
                synchronized (sessions.get(reciever)) {
                    sessions.get(reciever).sendMessage(new TextMessage(msg));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendTo(WSCLIENTS reciever, WSCLIENTS sender, String msg) {

        try {
            if (sessions.get(reciever) != null) {
                synchronized (sessions.get(reciever)) {
                    sessions.get(reciever).sendMessage(new TextMessage(sender.getWebsocketClientName() + ":" + msg));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BiMap<WSCLIENTS, WebSocketSession> getSessions() {
        return sessions;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage jsonTextMessage) throws Exception {
        String msg = jsonTextMessage.getPayload();

        if (msg.startsWith("Name")) {
            String[] name = msg.split(":");
            sessions.put(WSCLIENTS.valueOfByWSClientName(name[1]), session);
        }
        messageHandler(session, msg);
    }

    private void messageHandler(WebSocketSession session, String msg) {
        if (msg.startsWith("autonom")) {
            autonomManager.startAutnom();
            return;
        } else if (msg.startsWith("stoppAutonom")) {
            autonomManager.stopp();
            return;
        } else if (msg.startsWith("code")) {
            codeCompilerAndRunner.compileAndRunCode(msg);
            return;
        } else if (msg.contains("pressed") || msg.contains("released")) {
            String[] keyEvent = msg.split(":");
            keyMap.setKey(keyEvent[0], keyEvent[1].equals("pressed"));
            return;
        }
        RobotCurrentState.setSensorValueAutmaticale(msg);
    }
}
