package diy.robot.autonom;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
@Component
public class AutonomManager {

    static ExecutorService executorService = Executors.newFixedThreadPool(1);

    @Autowired
    AutonomMode autonomMode;

    Future autnomThread;

    public void startAutnom() {
        autonomMode.setIsInterrupted(false);
        autnomThread = runNewThread(() -> autonomMode.moveStuff());
    }

    public void stopp() {
        autonomMode.setIsInterrupted(true);
    }


    private Future runNewThread(Runnable runnable) {

        return executorService.submit(runnable);
    }

    public void setModus(String content) {
        if (content.equals("autonomOn")) {
            startAutnom();
        } else {
            stopp();
        }
    }
}
