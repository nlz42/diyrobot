package diy.robot.autonom;

import diy.robot.autonom.utility.RobotWebArmCommandHelper;
import diy.robot.robot.RobotCommandExecuter;
import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotCommand;
import diy.robot.robot.command.RobotLaserCommand;
import diy.robot.robot.command.RobotMoveCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CodeCompilerAndRunner {

    @Autowired
    RobotCommandExecuter executer;

    public void compileAndRunCode(String rawCode) {
        String code = rawCode.substring(5);
        String[] rawCodes = code.split("<br>");

        for (String rawCommand : rawCodes) {
            RobotCommand command = parseCode(rawCommand);
            if (command != null) {
                if (command instanceof RobotMoveCommand) {
                    executer.sendMoveCommand((RobotMoveCommand) command);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    executer.sendMoveCommand(new RobotMoveCommand(RobotMoveCommand.Direction.STOP, 0));
                }
                if (command instanceof RobotLaserCommand) {
                    executer.sendLaserCommad((RobotLaserCommand) command);
                }
                if (command instanceof RobotArmCommand) {
                    executer.sendRobotArmCommand((RobotArmCommand) command);
                }
            }
        }
    }

    private RobotCommand parseCode(String rawCode) {
        if (RobotMoveCommand.Direction.contains(rawCode)) {
            return new RobotMoveCommand(RobotMoveCommand.Direction.valueOf(rawCode), 80);
        }
        if (RobotWebArmCommandHelper.ARMCOMMAND.contains(rawCode)) {
            return RobotWebArmCommandHelper.buildRobotArmCommand(RobotWebArmCommandHelper.ARMCOMMAND.valueOf(rawCode));
        }
        if (RobotLaserCommand.LASER.contains(rawCode)) {
            return new RobotLaserCommand(RobotLaserCommand.LASER.valueOf(rawCode));
        }
        return null;
    }
}
