package diy.robot.autonom.utility;

import diy.robot.robot.RobotCurrentState;
import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotCommand;

public class RobotWebArmCommandHelper {

    public enum ARMCOMMAND {
        DOWN,
        UP,
        GRAP,
        OPEN;

        public static boolean contains(String test) {
            for (ARMCOMMAND c : ARMCOMMAND.values()) {
                if (c.name().equals(test)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static RobotCommand buildRobotArmCommand(RobotWebArmCommandHelper.ARMCOMMAND armcommand) {

        switch (armcommand) {
            case UP:
                return new RobotArmCommand(null, 0, new double[]{90.0, 90.0, 90.0, 90.0, 90.0, RobotCurrentState.getArmMotor6()});
            case DOWN:
                return new RobotArmCommand(null, 0, new double[]{92.5, 30, 45, 112.5, 2.5, RobotCurrentState.getArmMotor6()});
            case GRAP:
                return new RobotArmCommand(null, 0, new double[]{
                        RobotCurrentState.getArmMotor1(),
                        RobotCurrentState.getArmMotor2(),
                        RobotCurrentState.getArmMotor3(),
                        RobotCurrentState.getArmMotor4(),
                        RobotCurrentState.getArmMotor5(),
                        50.0});
            case OPEN:
                return new RobotArmCommand(null, 0, new double[]{
                        RobotCurrentState.getArmMotor1(),
                        RobotCurrentState.getArmMotor2(),
                        RobotCurrentState.getArmMotor3(),
                        RobotCurrentState.getArmMotor4(),
                        RobotCurrentState.getArmMotor5(),
                        142.0});
            default:
                return null;
        }
    }
}
