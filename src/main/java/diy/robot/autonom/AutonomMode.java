package diy.robot.autonom;

import diy.robot.robot.RobotCommandExecuter;
import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotLaserCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class AutonomMode {

    private boolean interrupted = false;
    private RobotCommandExecuter robotCommandExecuter;

    @Autowired
    public AutonomMode(RobotCommandExecuter robotCommandExecuter) {
        this.robotCommandExecuter = robotCommandExecuter;
    }

    public void moveStuff() {

        while (true) {
            try {
                robotCommandExecuter.sendLaserCommad(new RobotLaserCommand(RobotLaserCommand.LASER.ON));
                Thread.sleep(2000);
                robotCommandExecuter.sendLaserCommad(new RobotLaserCommand(RobotLaserCommand.LASER.OFF));
                Thread.sleep(2000);

                double[] position = {92.5, 30, 45, 112.5, 2.5, 142};

                robotCommandExecuter.sendRobotArmCommand(new RobotArmCommand(null, 0, position));

                Thread.sleep(1000);

                //Grap it:
                position[5] = 50.0;
                robotCommandExecuter.sendRobotArmCommand(new RobotArmCommand(null, 0, position));

                Thread.sleep(1000);
                //Pull it from floor
                position[0] = 90;
                position[1] = 90;
                position[2] = 90;
                position[3] = 90;
                position[4] = 90;
                robotCommandExecuter.sendRobotArmCommand(new RobotArmCommand(null, 0, position));

                for (int i = 0; i < 10; i++) {
                    robotCommandExecuter.sendLaserCommad(new RobotLaserCommand(RobotLaserCommand.LASER.ON));
                    Thread.sleep(300);
                    robotCommandExecuter.sendLaserCommad(new RobotLaserCommand(RobotLaserCommand.LASER.OFF));
                    Thread.sleep(300);
                }

                if (interrupted) {
                    log.error("Autnomer Modus wurde beendet");
                    return;
                }

                log.info("Atonomer Modus an! -> keep running till interupt!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setIsInterrupted(boolean isInterrupted) {
        interrupted = isInterrupted;
    }

}
