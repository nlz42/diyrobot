package diy.robot.robot;

import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotLaserCommand;
import diy.robot.robot.command.RobotMoveCommand;
import diy.robot.websocket.SocketMessageHandler;
import org.springframework.stereotype.Service;

@Service
public class RobotCommandExecuter {

    public void sendMoveCommand(RobotMoveCommand moveCommand) {
        SocketMessageHandler.sendTo(RobotMoveCommand.NAME, moveCommand.getDirection() + ":" + moveCommand.getSpeed());
    }

    public void sendLaserCommad(RobotLaserCommand laserCommand) {
        SocketMessageHandler.sendTo(RobotLaserCommand.NAME, laserCommand.getLaser().name());
    }

    public void sendRobotArmCommand(RobotArmCommand armCommand) {
        if (armCommand.getAllServoPositions() != null) {
            while (!checkIfArmReachPosition(armCommand.getAllServoPositions())) {
                SocketMessageHandler.sendTo(RobotArmCommand.NAME,
                        armCommand.getAllServoPositions()[0] + ":" +
                                armCommand.getAllServoPositions()[1] + ":" +
                                armCommand.getAllServoPositions()[2] + ":" +
                                armCommand.getAllServoPositions()[3] + ":" +
                                armCommand.getAllServoPositions()[4] + ":" +
                                armCommand.getAllServoPositions()[5]);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            SocketMessageHandler.sendTo(RobotArmCommand.NAME, armCommand.getArmMotor() + ":" + armCommand.getArmCommand());
        }
    }


    private boolean checkIfArmReachPosition(double[] motorFinalPosition) {

        return motorFinalPosition[0] == RobotCurrentState.getArmMotor1() &&
                motorFinalPosition[1] == RobotCurrentState.getArmMotor2() &&
                motorFinalPosition[2] == RobotCurrentState.getArmMotor3() &&
                motorFinalPosition[3] == RobotCurrentState.getArmMotor4() &&
                motorFinalPosition[4] == RobotCurrentState.getArmMotor5() &&
                motorFinalPosition[5] == RobotCurrentState.getArmMotor6();
    }
}
