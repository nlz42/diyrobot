package diy.robot.robot.command;

import diy.robot.websocket.WSCLIENTS;

public interface RobotCommand {

    WSCLIENTS getName();
}
