package diy.robot.robot.command;

import diy.robot.websocket.WSCLIENTS;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RobotMoveCommand implements RobotCommand {

    public enum Direction {
        FORWARD, BACKWARD, RIGHT, LEFT, CURVERIGHTFORWARD, CURVERIGHBACKWARD, CURVELEFTBACKWARD, CURVELEFTFORWARD, STOP;

        public static boolean contains(String test) {
            for (Direction c : Direction.values()) {
                if (c.name().equals(test)) {
                    return true;
                }
            }
            return false;
        }
    }

    public final static WSCLIENTS NAME = WSCLIENTS.MOTORCONTROLWS;

    private Direction direction;
    private int speed = 80;

    @Override
    public WSCLIENTS getName() {
        return NAME;
    }
}

