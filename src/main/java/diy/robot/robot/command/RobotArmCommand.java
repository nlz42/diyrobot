package diy.robot.robot.command;

import diy.robot.websocket.WSCLIENTS;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RobotArmCommand implements RobotCommand {

    public enum ArmCommand {
        INIT, CLOCKWISE, ANTICLOCKWISE, STOP
    }

    public final static WSCLIENTS NAME = WSCLIENTS.ROBOTARM;

    private ArmCommand armCommand;
    private int armMotor;
    private double[] allServoPositions;

    @Override
    public WSCLIENTS getName() {
        return NAME;
    }
}
