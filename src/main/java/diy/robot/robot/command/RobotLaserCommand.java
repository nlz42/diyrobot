package diy.robot.robot.command;

import diy.robot.websocket.WSCLIENTS;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RobotLaserCommand implements RobotCommand {

    public enum LASER {
        ON, OFF;

        public static boolean contains(String test) {
            for (LASER c : LASER.values()) {
                if (c.name().equals(test)) {
                    return true;
                }
            }
            return false;
        }
    }

    public final static WSCLIENTS NAME = WSCLIENTS.LASER;

    private LASER laser;

    @Override
    public WSCLIENTS getName() {
        return NAME;
    }
}
