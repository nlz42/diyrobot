package diy.robot.robot;

import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotLaserCommand;
import diy.robot.robot.command.RobotMoveCommand;
import diy.robot.websocket.SocketMessageHandler;
import diy.robot.websocket.WSCLIENTS;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import static diy.robot.websocket.WSCLIENTS.*;

@Data
@Slf4j
public class RobotCurrentState {

    private static double ultrasonicFront = 0.0;
    private static double ultrasonicBack = 0.0;

    private static boolean lightSensor = false;

    private static String rfidSensor = "";

    private static double armMotor1 = 90.0;
    private static double armMotor2 = 90.0;
    private static double armMotor3 = 90.0;
    private static double armMotor4 = 90.0;
    private static double armMotor5 = 90.0;
    private static double armMotor6 = 90.0;

    private static RobotMoveCommand.Direction direction = RobotMoveCommand.Direction.STOP;
    private static RobotArmCommand.ArmCommand ronotArm = RobotArmCommand.ArmCommand.INIT;
    private static RobotLaserCommand.LASER laser = RobotLaserCommand.LASER.OFF;

    private static double batteryVoltage = 0.0;

    public static void setSensorValueAutmaticale(String sensor) {
        String[] cursensor = sensor.split(":");
        WSCLIENTS client = WSCLIENTS.valueOfByWSClientName(cursensor[0]);
        switch (client) {
            case LASER:
                laser = cursensor[1].equals("ON") ? RobotLaserCommand.LASER.ON : RobotLaserCommand.LASER.OFF;
                SocketMessageHandler.sendTo(WEBCLIENT, LASER, laser.name());
                break;
            case MOTORCONTROLWS:
                direction = RobotMoveCommand.Direction.valueOf(cursensor[1]);
                SocketMessageHandler.sendTo(WEBCLIENT, MOTORCONTROLWS, direction.name());
                break;
            case ULTRASONICFRONT:
                ultrasonicFront = Double.parseDouble(cursensor[1]);
                SocketMessageHandler.sendTo(WEBCLIENT, ULTRASONICFRONT, String.valueOf(ultrasonicFront));
                break;
            case ULTRASONICBACK:
                ultrasonicBack = Double.parseDouble(cursensor[1]);
                SocketMessageHandler.sendTo(WEBCLIENT, ULTRASONICBACK, String.valueOf(ultrasonicBack));
                break;
            case LIGHTSENSOR:
                lightSensor = cursensor[1].equals("0");
                SocketMessageHandler.sendTo(WEBCLIENT, LIGHTSENSOR, String.valueOf(lightSensor));
                break;
            case RFIDSENSOR:
                rfidSensor = cursensor[1];
                SocketMessageHandler.sendTo(WEBCLIENT, RFIDSENSOR, rfidSensor);
                break;
            case ROBOTARM:
                String[] values = cursensor[1].split("#");
                setMotorValuesInVariable(values);
                SocketMessageHandler.sendTo(WEBCLIENT, ROBOTARM, armMotor1 + ":" + armMotor2
                        + ":" + armMotor3 + ":" + armMotor4 + ":" + armMotor5 + ":" + armMotor6);
                break;
            case BATTERYVOLTAGE: //TODO not active
                double value = Double.parseDouble(cursensor[1]);
                batteryVoltage = value;
                SocketMessageHandler.sendTo(WEBCLIENT, BATTERYVOLTAGE, String.valueOf(batteryVoltage));
                break;
            default:
                //Just ignore it than :-)
        }
    }

    private static void setMotorValuesInVariable(String[] values) {
        if (values.length < 6) {
            log.error("Somthing went Wrong, not enough Robot arm Motors");
        }
        armMotor1 = Double.valueOf(values[0]);
        armMotor2 = Double.valueOf(values[1]);
        armMotor3 = Double.valueOf(values[2]);
        armMotor4 = Double.valueOf(values[3]);
        armMotor5 = Double.valueOf(values[4]);
        armMotor6 = Double.valueOf(values[5]);
    }

    public static double getUltrasonicFront() {
        return ultrasonicFront;
    }

    public static double getUltrasonicBack() {
        return ultrasonicBack;
    }

    public static boolean isLightSensor() {
        return lightSensor;
    }

    public static String getRfidSensor() {
        return rfidSensor;
    }

    public static double getArmMotor1() {
        return armMotor1;
    }

    public static double getArmMotor2() {
        return armMotor2;
    }

    public static double getArmMotor3() {
        return armMotor3;
    }

    public static double getArmMotor4() {
        return armMotor4;
    }

    public static double getArmMotor5() {
        return armMotor5;
    }

    public static double getArmMotor6() {
        return armMotor6;
    }
}
