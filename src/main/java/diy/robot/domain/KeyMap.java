package diy.robot.domain;

import diy.robot.robot.RobotCommandExecuter;
import diy.robot.robot.command.RobotArmCommand;
import diy.robot.robot.command.RobotLaserCommand;
import diy.robot.robot.command.RobotMoveCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import static diy.robot.robot.command.RobotArmCommand.ArmCommand.STOP;
import static diy.robot.robot.command.RobotArmCommand.ArmCommand.*;
import static diy.robot.robot.command.RobotLaserCommand.LASER.OFF;
import static diy.robot.robot.command.RobotLaserCommand.LASER.ON;
import static diy.robot.robot.command.RobotMoveCommand.Direction.*;


@Component
@Slf4j
public class KeyMap {

    private static HashMap<String, Boolean> keyStatus = new HashMap<>();

    private RobotCommandExecuter commandExecuter;

    @Autowired
    public KeyMap(RobotCommandExecuter commandExecuter) {
        this.commandExecuter = commandExecuter;
        // RobotMoveMotor
        keyStatus.put("W", false);
        keyStatus.put("A", false);
        keyStatus.put("S", false);
        keyStatus.put("D", false);
        // RobotArm
        keyStatus.put("1", false);
        keyStatus.put("2", false);
        keyStatus.put("3", false);
        keyStatus.put("4", false);
        keyStatus.put("5", false);
        keyStatus.put("6", false);
        keyStatus.put("9", false);//9 for clockwise
        keyStatus.put("0", false);//0 for anticlockwise
        keyStatus.put("I", false);
        // Laser
        keyStatus.put("E", false);
    }

    public void setKey(String key, Boolean pressed) {
        try {
            if (keyStatus.get(key) != null) {
                keyStatus.put(key, pressed);
                calcDirection();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calcDirection() {

        if (keyStatus.get("W") && !keyStatus.get("A") && !keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(FORWARD, 80));
        } else if (!keyStatus.get("W") && keyStatus.get("A") && !keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(LEFT, 80));
        } else if (!keyStatus.get("W") && !keyStatus.get("A") && keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(BACKWARD, 80));
        } else if (!keyStatus.get("W") && !keyStatus.get("A") && !keyStatus.get("S") && keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(RIGHT, 80));
        } else if (keyStatus.get("W") && keyStatus.get("A") && !keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(CURVELEFTFORWARD, 80));
        } else if (keyStatus.get("W") && !keyStatus.get("A") && !keyStatus.get("S") && keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(CURVERIGHTFORWARD, 80));
        } else if (!keyStatus.get("W") && keyStatus.get("A") && keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(CURVELEFTBACKWARD, 80));
        } else if (!keyStatus.get("W") && !keyStatus.get("A") && keyStatus.get("S") && keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(CURVERIGHBACKWARD, 80));
        } else if (!keyStatus.get("W") && !keyStatus.get("A") && !keyStatus.get("S") && !keyStatus.get("D")) {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(RobotMoveCommand.Direction.STOP, 80));
        } else {
            commandExecuter.sendMoveCommand(new RobotMoveCommand(RobotMoveCommand.Direction.STOP, 80));
        }

        if (keyStatus.get("1") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 0, null));
        } else if (keyStatus.get("1") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 0, null));
        } else if (keyStatus.get("2") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 1, null));
        } else if (keyStatus.get("2") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 1, null));
        } else if (keyStatus.get("3") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 2, null));
        } else if (keyStatus.get("3") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 2, null));
        } else if (keyStatus.get("4") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 3, null));
        } else if (keyStatus.get("4") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 3, null));
        } else if (keyStatus.get("5") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 4, null));
        } else if (keyStatus.get("5") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 4, null));
        } else if (keyStatus.get("6") && keyStatus.get("9")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(CLOCKWISE, 5, null));
        } else if (keyStatus.get("6") && keyStatus.get("0")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(ANTICLOCKWISE, 5, null));
        } else {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(STOP, 0, null));
        }

        if (keyStatus.get("I")) {
            commandExecuter.sendRobotArmCommand(new RobotArmCommand(INIT, 0, null));
        }

        if (keyStatus.get("E")) {
            commandExecuter.sendLaserCommad(new RobotLaserCommand(ON));
        } else {
            commandExecuter.sendLaserCommad(new RobotLaserCommand(OFF));
        }
    }
}
