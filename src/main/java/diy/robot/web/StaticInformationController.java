package diy.robot.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class StaticInformationController {

    private static List<WebMenu> menuEntrsies = new ArrayList<>();

    static {
        menuEntrsies.add(new WebMenu("Home", "/"));
        menuEntrsies.add(new WebMenu("Dokumentation", "https://robot.nlz-it.net/"));
    }

    @ModelAttribute
    public void setAllStaticInformation(Model model) {

        model.addAttribute("menu", menuEntrsies);
    }
}
