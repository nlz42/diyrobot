package diy.robot.web;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WebMenu {

    private String title;
    private String href;
}
