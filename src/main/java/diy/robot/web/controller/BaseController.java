package diy.robot.web.controller;

import diy.robot.drivers.PythonThreadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@Slf4j
public class BaseController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        PythonThreadService.startWatchdog();
        return "index";
    }
}
