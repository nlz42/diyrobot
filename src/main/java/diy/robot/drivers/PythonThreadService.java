package diy.robot.drivers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
@Slf4j
public class PythonThreadService {

    static ExecutorService executorService = Executors.newFixedThreadPool(8);

    private static Process pythonMotor;
    private static Process pythonUltrasonicFront;
    private static Process pythonUltrasonicBack;
    private static Process pythonLightSensor;
    private static Process pythonLaser;
    private static Process pythonRobotArm;
    private static Process pythonRfidReader;

    private static Boolean watchdogStarted = false;

    public static void restartUltrasonicFront() {
        if (pythonUltrasonicFront != null) {
            pythonUltrasonicFront.destroy();
        }
        startPythonUltrasonicFront();
    }

    public static void restartUltrasonicBack() {
        if (pythonUltrasonicBack != null) {
            pythonUltrasonicBack.destroy();
        }
        startPythonUltrasonicBack();

    }

    public static void stoppAllServices() {
        if (pythonMotor != null
                && pythonUltrasonicFront != null
                && pythonUltrasonicBack != null
                && pythonLightSensor != null
                && pythonLaser != null
                && pythonRobotArm != null
                && pythonRfidReader != null) {
            pythonMotor.destroy();
            pythonUltrasonicFront.destroy();
            pythonUltrasonicBack.destroy();
            pythonLightSensor.destroy();
            pythonLaser.destroy();
            pythonRobotArm.destroy();
            pythonRfidReader.destroy();
            executorService.shutdown();
            executorService = Executors.newFixedThreadPool(8);
        }
    }

    public static void startWatchdog() {

        if (!watchdogStarted) {
            log.info("##### RUN new WATCHDOG ########");
            runNewThread(() -> {
                Watchdog.watchdog();
            });
            watchdogStarted = true;

        }
    }

    public static void restartAllThreads() {
        stoppAllServices();
        startPythonMotorThread();
        startPythonUltrasonicFront();
        startPythonUltrasonicBack();
        startPythonLightSensor();
        startLaser();
        startRobotArm();
        startRFIDReader();
    }

    /**
     * Motors
     */
    public static void startPythonMotorThread() {

        runNewThread(() -> {
            pythonMotor = PythonPathAndProcessManager.startMotorControllListener();
        });
    }

    /**
     * Sensor Ultrasonic
     */
    public static void startPythonUltrasonicFront() {

        runNewThread(() -> {
            pythonUltrasonicFront = PythonPathAndProcessManager.startDistanceMessureFront();

        });
    }

    /**
     * Sensor Ultrasonic
     */
    public static void startPythonUltrasonicBack() {

        runNewThread(() -> {
            pythonUltrasonicBack = PythonPathAndProcessManager.startDistanceMessureBack();
        });
    }

    /**
     * Start robot Arm
     */
    public static void startRobotArm() {
        runNewThread(() -> {
            pythonRobotArm = PythonPathAndProcessManager.startRobotArm();
        });
    }

    /**
     * Sensor Light
     */
    public static void startPythonLightSensor() {

        runNewThread(() -> {
            pythonLightSensor = PythonPathAndProcessManager.startlightMessure();
        });
    }

    /**
     * Start Laser
     */
    public static void startLaser() {

        runNewThread(() -> {
            pythonLaser = PythonPathAndProcessManager.startLaser();
        });
    }

    /**
     * Start RFID Reader
     */
    public static void startRFIDReader() {

        runNewThread(() -> {
            pythonRfidReader = PythonPathAndProcessManager.startRFIDReader();
        });
    }

    public static void runNewThread(Runnable runnable) {
        executorService.execute(runnable);
    }

    /*
     * Just in case we need some day a thread service with return values
     */
    private Future<Object> runThreadWithReturnValue(Callable callable) {
        return executorService.submit(callable);
    }
}
