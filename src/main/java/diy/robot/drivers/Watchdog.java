package diy.robot.drivers;

import diy.robot.robot.RobotCurrentState;
import diy.robot.websocket.SocketMessageHandler;
import diy.robot.websocket.WSCLIENTS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Watchdog {

    private static double oldUltrasonicBack = 0.0;
    private static double oldUltrasonicFront = 0.0;

    public static void watchdog() {

        //Check if one client is missing!
        if (SocketMessageHandler.getSessions().size() < 8) {
            log.error("Some clients are missing, strart all threads new");
            PythonThreadService.restartAllThreads();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        while (true) {
            try {
                Thread.sleep(10000);
                if (RobotCurrentState.getUltrasonicBack() == oldUltrasonicBack || RobotCurrentState.getUltrasonicFront() == oldUltrasonicFront) {
                    PythonThreadService.restartUltrasonicBack();
                    PythonThreadService.restartUltrasonicFront();
                    log.error("Restart untrasonic sensors!");
                } else {
                    oldUltrasonicBack = RobotCurrentState.getUltrasonicBack();
                    oldUltrasonicFront = RobotCurrentState.getUltrasonicFront();
                    log.info("All seems okay");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void restartDisconnectClient(WSCLIENTS wsclients) {
        switch (wsclients) {
            case LASER:
                PythonThreadService.startLaser();
                return;
            case MOTORCONTROLWS:
                PythonThreadService.startPythonMotorThread();
                return;
            case LIGHTSENSOR:
                PythonThreadService.startPythonLightSensor();
                return;
            case RFIDSENSOR:
                PythonThreadService.startRFIDReader();
                return;
            case ROBOTARM:
                PythonThreadService.startRobotArm();
                return;
            default:
                log.error("Could not find client to restart: {}", wsclients);
        }
    }
}
