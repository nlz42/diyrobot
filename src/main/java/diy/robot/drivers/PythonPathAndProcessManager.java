package diy.robot.drivers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Slf4j
@Component
public class PythonPathAndProcessManager {

    private static String pathRel = "";

    public static Process startDistanceMessureBack() {
        setRealtivePythonPath();
        ProcessBuilder ultrasonicPocessBuilder = new ProcessBuilder("python3",
                pathRel + "/python/ultrasonicBack/ws-ultrasonic.py");
        Process ultrasonicProcess = null;

        try {
            ultrasonicProcess = ultrasonicPocessBuilder.start();
            log.info("Python Distance-Messure started BACK");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultrasonicProcess;
    }

    public static Process startDistanceMessureFront() {
        setRealtivePythonPath();
        ProcessBuilder ultrasonicPocessBuilder = new ProcessBuilder("python3",
                pathRel + "/python/ultrasonicFront/ws-ultrasonic.py");
        Process ultrasonicProcess = null;

        try {
            ultrasonicProcess = ultrasonicPocessBuilder.start();
            log.info("Python Distance-Messure started");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultrasonicProcess;
    }

    public static Process startMotorControllListener() {
        setRealtivePythonPath();
        ProcessBuilder motorPocessBuilder = new ProcessBuilder("python3",
                pathRel + "/python/motorSteuerung/ws-DCMotorSteuerung.py");
        Process motorPocess = null;

        try {
            motorPocess = motorPocessBuilder.start();
            log.info("Python motorClient started");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return motorPocess;
    }

    public static Process startlightMessure() {
        setRealtivePythonPath();
        ProcessBuilder lightProcessBuilder = new ProcessBuilder("python3",
                pathRel + "/python/lightSensor/ws-light_sens.py");
        Process lightProcess = null;

        try {
            lightProcess = lightProcessBuilder.start();
            System.out.println("Python Light-Messure started");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lightProcess;
    }

    public static Process startRobotArm() {
        setRealtivePythonPath();
        ProcessBuilder robotArmPocessBuilder = new ProcessBuilder("python3", pathRel + "/python/robotArm/ws-client.py");
        Process robotArmProcess = null;

        try {
            robotArmProcess = robotArmPocessBuilder.start();
            log.info("Python robot arm started");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return robotArmProcess;
    }

    public static Process startLaser() {
        setRealtivePythonPath();
        ProcessBuilder laserPocessBuilder = new ProcessBuilder("python3", pathRel + "/python/laser/ws-laser-client.py");
        Process laserPocess = null;

        try {
            laserPocess = laserPocessBuilder.start();
            log.info("Python laser started");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return laserPocess;
    }

    public static Process startRFIDReader() {
        setRealtivePythonPath();
        ProcessBuilder rfidPocessBuilder = new ProcessBuilder("python3", pathRel + "/python/rfidReader/rfid-ws-client.py");
        Process rfidPocess = null;

        try {
            rfidPocess = rfidPocessBuilder.start();
            log.info("Python RFID started");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rfidPocess;
    }

    private static void setRealtivePythonPath() {
        final Class<?> referenceClass = PythonPathAndProcessManager.class;
        final URL url = referenceClass.getProtectionDomain().getCodeSource().getLocation();
        if (url.getPath().contains(".jar")) {
            // execution jar Time
            String[] path = url.getPath().split(".jar");
            pathRel = path[0].substring(5, path[0].lastIndexOf("/")); // delete file: (5) end End /DIY-Sna.....jar
            System.out.println(pathRel);
        } else { // here for Debuug Mopde!
            File file = new File(url.getPath()).getParentFile();
            System.out.println(pathRel);
            pathRel = file.getPath();
        }
    }
}
