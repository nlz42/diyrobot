import asyncio
import websockets
import DC_Motor

async def wsclient():
	async with websockets.connect('ws://localhost:8090/ws-robot') as websocket:
		motor = DC_Motor.DC_Motor()
		await websocket.send('Name:'+'motorControllWS')
		while True:
			msg = await websocket.recv()
			values = msg.split(':')
			motor.setMotorOrder(values[0],values[1])
			await websocket.send('motorControllWS:'+values[0])
			#print(values)

#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient())
