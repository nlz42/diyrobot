import time
import asyncio
import sys, getopt
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

GPIO_PWM_LEFT = 13
GPIO_LEFT_V = 5
GPIO_LEFT_GRND = 6

GPIO_PWM_RIGHT = 17
GPIO_RIGHT_V = 27
GPIO_RIGHT_GRND = 22

class DC_Motor:

#FORWARD, BACKWARD, RIGHT, LEFT, CURVERIGHTFORWARD, CURVERIGHBACKWARD, CURVELEFTBACKWARD, CURVELEFTFORWARD, STOP


    def __init__(self):

        global GPIO_PWM_LEFT
        global GPIO_LEFT_V
        global GPIO_LEFT_GRND

        global GPIO_PWM_RIGHT
        global GPIO_RIGHT_V
        global GPIO_RIGHT_GRND

        #Pins Motor A -> MotorLeft
        GPIO.setup(GPIO_PWM_LEFT, GPIO.OUT)#PWM
        GPIO.setup(GPIO_LEFT_V, GPIO.OUT) # V - Forward, High
        GPIO.setup(GPIO_LEFT_GRND, GPIO.OUT) # Grnd - Forward, LOW

        #Pins Motor B -> MotorRight
        GPIO.setup(GPIO_PWM_RIGHT, GPIO.OUT) #PWM
        GPIO.setup(GPIO_RIGHT_V, GPIO.OUT) # V -Forward, High
        GPIO.setup(GPIO_RIGHT_GRND, GPIO.OUT) # Grnd - Forward, LOW
        #PWM for Controll
        self.PWM_LEFT = GPIO.PWM(GPIO_PWM_LEFT, 100)
        self.PWM_RIGHT = GPIO.PWM(GPIO_PWM_RIGHT, 100)

        pause = 0.01 # time between pwn signal
        self.PWM_LEFT.start(0)
        self.PWM_RIGHT.start(0)
        #currentDirection
        self.speed = 0
        self.diffForCurve = 20

    def setMotorOrder(self,direction,speed):
        self.speed = float(speed)
        if direction == 'FORWARD':
            self.forward()
        elif direction == 'BACKWARD':
            self.backward()
        elif direction == 'RIGHT':
            self.right()
        elif direction == 'LEFT':
            self.left()
        elif direction == 'CURVERIGHTFORWARD':
            self.curverightforward()
        elif direction == 'CURVERIGHBACKWARD':
            self.curverighbackward()
        elif direction == 'CURVELEFTBACKWARD':
            self.curveleftbackward()
        elif direction == 'CURVELEFTFORWARD':
            self.curveleftforward()
        elif direction == 'STOP':
            self.stop()
        else:
            print ('Directio not Implement')


    def stop(self):
        self.PWM_LEFT.ChangeDutyCycle(0)
        self.PWM_RIGHT.ChangeDutyCycle(0)
        GPIO.output(GPIO_LEFT_V, GPIO.LOW)
        GPIO.output(GPIO_RIGHT_V, GPIO.LOW)
        GPIO.output(GPIO_LEFT_GRND, GPIO.LOW)
        GPIO.output(GPIO_RIGHT_GRND, GPIO.LOW)


    def forward(self):
        GPIO.output(GPIO_LEFT_V, GPIO.HIGH)
        GPIO.output(GPIO_RIGHT_V, GPIO.HIGH)
        GPIO.output(GPIO_LEFT_GRND, GPIO.LOW)
        GPIO.output(GPIO_RIGHT_GRND, GPIO.LOW)
        self.PWM_LEFT.ChangeDutyCycle(self.speed)
        self.PWM_RIGHT.ChangeDutyCycle(self.speed)

    def backward(self):
        GPIO.output (GPIO_LEFT_V, GPIO.LOW)
        GPIO.output (GPIO_RIGHT_V, GPIO.LOW)
        GPIO.output (GPIO_LEFT_GRND, GPIO.HIGH)
        GPIO.output (GPIO_RIGHT_GRND, GPIO.HIGH)
        self.PWM_LEFT.ChangeDutyCycle( self.speed)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed)


    def left(self):
        GPIO.output (GPIO_LEFT_V, GPIO.HIGH)
        GPIO.output (GPIO_RIGHT_V, GPIO.LOW)
        GPIO.output (GPIO_LEFT_GRND, GPIO.LOW)
        GPIO.output (GPIO_RIGHT_GRND, GPIO.HIGH)
        self.PWM_LEFT.ChangeDutyCycle( self.speed)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed)

    def right(self):
        GPIO.output (GPIO_LEFT_V, GPIO.LOW)
        GPIO.output (GPIO_RIGHT_V, GPIO.HIGH)
        GPIO.output (GPIO_LEFT_GRND, GPIO.HIGH)
        GPIO.output (GPIO_RIGHT_GRND, GPIO.LOW)
        self.PWM_LEFT.ChangeDutyCycle( self.speed)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed)

    def curverightforward(self): # CURVERIGHTFORWARD
        GPIO.output(GPIO_LEFT_V, GPIO.HIGH)
        GPIO.output(GPIO_RIGHT_V, GPIO.HIGH)
        GPIO.output(GPIO_LEFT_GRND, GPIO.LOW)
        GPIO.output(GPIO_RIGHT_GRND, GPIO.LOW)
        self.PWM_LEFT.ChangeDutyCycle( self.speed-self.diffForCurve)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed+self.diffForCurve)

    def curveleftforward(self): # CURVELEFTFORWARD
        GPIO.output(GPIO_LEFT_V, GPIO.HIGH)
        GPIO.output(GPIO_RIGHT_V, GPIO.HIGH)
        GPIO.output(GPIO_LEFT_GRND, GPIO.LOW)
        GPIO.output(GPIO_RIGHT_GRND, GPIO.LOW)
        self.PWM_LEFT.ChangeDutyCycle( self.speed+self.diffForCurve)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed-self.diffForCurve)

    def curverighbackward(self): # CURVERIGHBACKWARD
        GPIO.output (GPIO_LEFT_V, GPIO.LOW)
        GPIO.output (GPIO_RIGHT_V, GPIO.LOW)
        GPIO.output (GPIO_LEFT_GRND, GPIO.HIGH)
        GPIO.output (GPIO_RIGHT_GRND, GPIO.HIGH)
        self.PWM_LEFT.ChangeDutyCycle( self.speed-self.diffForCurve)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed+self.diffForCurve)

    def curveleftbackward(self): # CURVELEFTBACKWARD
        GPIO.output (GPIO_LEFT_V, GPIO.LOW)
        GPIO.output (GPIO_RIGHT_V, GPIO.LOW)
        GPIO.output (GPIO_LEFT_GRND, GPIO.HIGH)
        GPIO.output (GPIO_RIGHT_GRND, GPIO.HIGH)
        self.PWM_LEFT.ChangeDutyCycle( self.speed+self.diffForCurve)
        self.PWM_RIGHT.ChangeDutyCycle( self.speed-self.diffForCurve)
