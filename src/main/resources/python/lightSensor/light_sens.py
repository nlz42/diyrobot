#Bibliotheken einbinden
import RPi.GPIO as GPIO
import time

#GPIO Modus (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#GPIO Pins zuweisen
GPIO_light= 26

#Richtung der GPIO-Pins festlegen (IN / OUT)
GPIO.setup(GPIO_light, GPIO.IN)

def light():
  lightsensor =  GPIO.input(GPIO_light)
  return lightsensor


if __name__ == '__main__':
  try:
    while True:
      lightValue = light()
      print ("Licht "+ str(lightValue))
      time.sleep(1)
  except KeyboardInterrupt:
    print("Messung vom User gestoppt")
    GPIO.cleanup()
