#!/usr/bin/env python

import asyncio
import websockets
import light_sens
import time

async def wsclient():
	async with websockets.connect('ws://localhost:8090/ws-robot') as websocket:
        #FirstMessaging - Name
		await websocket.send('Name:'+'lightSensor')
		while True:
			light = light_sens.light()

			await websocket.send('lightSensor:'+str(light))
			#print ('sending')
			time.sleep(1.0)


#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient())
