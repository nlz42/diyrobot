#!/usr/bin/env python

import asyncio
import websockets
import RobotArmControll

async def wsclient(callback):
	async with websockets.connect('ws://localhost:8090/ws-robot') as websocket:
		await websocket.send('Name:'+'robotArm')
		while True:
			msg = await websocket.recv()
			values = msg.split(':')
			await callback(msg, websocket)

async def myCustomCode(wsevent, websocket):
		key = wsevent.split(':') # [0] motor [1] clockOrAntoClockwise
		if len(key) > 5:
			motorPositions = RobotArmControll.movingAutnomMode(key[0],key[1],key[2],key[3],key[4],key[5])
		else:
			motorPositions = RobotArmControll.movingByKeyEvent( key[0],key[1])
		#print(motorPositions)
		await websocket.send('robotArm:'+motorPositions)
#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient(myCustomCode))
