from __future__ import division
import time
import Adafruit_PCA9685

pwm = Adafruit_PCA9685.PCA9685(address=0x41)
pwm.set_pwm_freq(50)

Motor1 = 0
Motor2 = 1
Motor3 = 2
Motor4 = 3
Motor5 = 4
Motor6 = 5
Motor1CurPos= 90
Motor2CurPos= 90
Motor3CurPos= 90
Motor4CurPos= 90
Motor5CurPos= 90
Motor6CurPos= 90

def set_servo_pulse(channel, gradiant):
    #grad in wert zwischen 0,5 und 2,5 umrechen
    pulse = gradiant*0.011+0.5
    pulse_length = 1000000
    pulse_length /= 50
    pulse_length /= 4096
    pulse *= 1000
    pulse /= pulse_length
    pulse = round(pulse)
    pulse = int(pulse)
    pwm.set_pwm(channel, 0, pulse)

#init to StartPositionen

def init():
    global Motor1CurPos
    global Motor2CurPos
    global Motor3CurPos
    global Motor4CurPos
    global Motor5CurPos
    global Motor6CurPos
    set_servo_pulse(Motor1,90)
    #time.sleep(0.5)
    set_servo_pulse(Motor2,90)
    #time.sleep(0.5)
    set_servo_pulse(Motor3,90)
    #time.sleep(0.5)
    set_servo_pulse(Motor4,90)
    #time.sleep(0.5)
    set_servo_pulse(Motor5,90)
    #time.sleep(0.5)
    set_servo_pulse(Motor6,90)
    #time.sleep(0.5)
    print('Calibrating Robot Arm')
    time.sleep(1)
    print ('Calibration Complete')
    Motor1CurPos= 90
    Motor2CurPos= 90
    Motor3CurPos= 90
    Motor4CurPos= 90
    Motor5CurPos= 90
    Motor6CurPos= 90

# Minimalwerte
Motor1Minimum = 0
Motor2Minimum = 0
Motor3Minimum = 0
Motor4Minimum = 20
Motor5Minimum = 0
Motor6Minimum = 45

# Maximalwerte
Motor1Maximum = 180
Motor2Maximum = 180
Motor3Maximum = 180
Motor4Maximum = 180
Motor5Maximum = 180
Motor6Maximum = 145

#TaktungAutoModus = 0.008 #sleep betwenn new PWM signal
margin = 0.5 # auf ein grad genaue

superSchnell = 5.0
schnell = 2.5
mittel  = 0.7
langsam = 0.4


def MotorBewegungAuto (Motor,CurPosition,MotorMinimum,MotorMaximum,FinalPosition,Geschwindigkeit):

    if abs(CurPosition-FinalPosition) <= margin:
        CurPosition = FinalPosition
        return CurPosition

    # Meldet, wenn sich die Motoren unter ihr Minimum/Maximum bewegen
    if CurPosition < MotorMinimum:
        CurPosition = CurPosition+1;
        return CurPosition

    if CurPosition > MotorMaximum:
        CurPosition = CurPosition-1;
        return CurPosition

    # Steuert den Motor
    if (CurPosition >= FinalPosition):
            CurPosition = CurPosition - Geschwindigkeit
            set_servo_pulse(Motor, CurPosition)
            return CurPosition

    elif (CurPosition < FinalPosition):
            CurPosition = CurPosition + Geschwindigkeit
            set_servo_pulse(Motor, CurPosition)
            return CurPosition


def moving(motor, position, geschwindigkeit):
    global Motor1CurPos
    global Motor2CurPos
    global Motor3CurPos
    global Motor4CurPos
    global Motor5CurPos
    global Motor6CurPos
    if (motor == 0):
        Motor1CurPos = MotorBewegungAuto(Motor1,Motor1CurPos,Motor1Minimum,Motor1Maximum,position,geschwindigkeit);
        #return Motor1CurPos
    if (motor == 1):
        Motor2CurPos = MotorBewegungAuto(Motor2,Motor2CurPos,Motor2Minimum,Motor2Maximum,position,geschwindigkeit);
        #return Motor2CurPos
    if (motor == 2):
        Motor3CurPos = MotorBewegungAuto(Motor3,Motor3CurPos,Motor3Minimum,Motor3Maximum,position,geschwindigkeit);
        #return Motor3CurPos
    if (motor == 3):
        Motor4CurPos = MotorBewegungAuto(Motor4,Motor4CurPos,Motor4Minimum,Motor4Maximum,position,geschwindigkeit);
        #return Motor4CurPos
    if (motor == 4):
        Motor5CurPos = MotorBewegungAuto(Motor5,Motor5CurPos,Motor5Minimum,Motor5Maximum,position,geschwindigkeit);
        #return Motor5CurPos
    if (motor == 5):
        Motor6CurPos = MotorBewegungAuto(Motor6,Motor6CurPos,Motor6Minimum,Motor6Maximum,position,geschwindigkeit);
        #return Motor6CurPos
    return

def movingByKeyEvent(motor, direction):
     global Motor1CurPos
     global Motor2CurPos
     global Motor3CurPos
     global Motor4CurPos
     global Motor5CurPos
     global Motor6CurPos
    
     motor = int(motor)
     
     if(direction == 'INIT'):
         init();
     if (direction == 'ANTICLOCKWISE'):
         moving(motor, 0, schnell)
     if (direction == 'CLOCKWISE'):
         moving(motor, 180, schnell)
    
     return str(Motor1CurPos)+'#'+str(Motor2CurPos)+'#'+str(Motor3CurPos)+'#'+str(Motor4CurPos)+'#'+str(Motor5CurPos)+'#'+str(Motor6CurPos);

def movingAutnomMode(pos1,pos2,pos3,pos4,pos5,pos6):
    global Motor1CurPos
    global Motor2CurPos
    global Motor3CurPos
    global Motor4CurPos
    global Motor5CurPos
    global Motor6CurPos

    print (pos1,pos2,pos3)

    moving(Motor1, float(pos1), schnell)
    moving(Motor2, float(pos2), schnell)
    moving(Motor3, float(pos3), schnell)
    moving(Motor4, float(pos4), schnell)
    moving(Motor5, float(pos5), schnell)
    moving(Motor6, float(pos6), schnell)

    return str(Motor1CurPos)+'#'+str(Motor2CurPos)+'#'+str(Motor3CurPos)+'#'+str(Motor4CurPos)+'#'+str(Motor5CurPos)+'#'+str(Motor6CurPos);

init()
time.sleep(1)

