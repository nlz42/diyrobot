import asyncio
import websockets
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(12, GPIO.OUT)

async def wsclient():
    async with websockets.connect('ws://localhost:8090/ws-robot') as websocket:
        await websocket.send('Name:'+'laser')
        GPIO.output(12, 0) #set laser off
        while True:
            msg = await websocket.recv()
            if (msg == "ON"):
                GPIO.output(12, 1)
            else:
                GPIO.output(12,0)
            await websocket.send('laser:'+msg)


#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient())