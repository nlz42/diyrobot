#!/usr/bin/env python

import asyncio
import websockets
import time

async def wsclient():
    async with websockets.connect('ws://localhost:9000') as websocket:
        #FirstMessaging - Name
        await websocket.send('Name:'+'ContSender')
        while True:
                await websocket.send('cont. sending')
                time.sleep(1.5)



#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient())
