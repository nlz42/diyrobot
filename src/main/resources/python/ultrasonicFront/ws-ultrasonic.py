#!/usr/bin/env python

import asyncio
import websockets
import ultrasonic
import time

async def wsclient():
	async with websockets.connect('ws://localhost:8090/ws-robot') as websocket:
        #FirstMessaging - Name
		await websocket.send('Name:'+'ultrasonicFront')
		while True:
			dis = ultrasonic.distanz()
			#dis=1234

			await websocket.send('ultrasonicFront:'+str(dis))
			#print ('sending')
			time.sleep(0.5)


#This have to be the last line!
asyncio.get_event_loop().run_until_complete(wsclient())
