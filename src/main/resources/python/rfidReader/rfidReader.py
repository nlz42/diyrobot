import RPi.GPIO as GPIO
import SimpleMFRC522
import time

reader = SimpleMFRC522.SimpleMFRC522()

def read():

  try:
    id, text = reader.read()
    return text
  finally:
    GPIO.cleanup()
    
if __name__ == '__main__':
    try:
        while True:
            rfidRead = read()
            #print("Id is: "+ str(id))
            print("Value is:"+rfidRead)
            time.sleep(1)

        # Beim Abbruch durch STRG+C resetten
    except KeyboardInterrupt:
        print("Messung vom User gestoppt")
        GPIO.cleanup()
