var websocket = null;

var globalCheckAutnomerModus = false;

var imageCounter = 0;

var images = [
    "../img/roboFace.gif"
]

var i = 0;

function slideshowPicture() {
    $("#webcam").attr(
        "src",
        images[i % images.length]);
    i++;
}

function checkImageExists(imageUrl, callBack) {
    var imageData = new Image();
    imageData.onload = function () {
        callBack(true);
    };
    imageData.onerror = function () {
        callBack(false);
    };
    imageData.src = imageUrl;
}

function disconnect() {
    websocket.close();
}

function connect() {
    try {
        websocket = new WebSocket( "ws://"
            + window.location.hostname + ":" + window.location.port
            + "/ws-robot");

        websocket.onopen = function (event) {
            websocket.send("Name:webclient");
            $("#connected").attr("class", "alert alert-success");
            $("#connected").text("Connected");
        };
        websocket.onmessage = function(e) {
        console.log('message received: ' + e.data);
           var res = e.data.split(":");
       if (res[0] == "ultrasonicFront") {
            $("#ultrasonicFront").text(res[1]);
        }
        if (res[0] == "ultrasonicBack") {
            $("#ultrasonicBack").text(res[1]);
        }
        if (res[0] == "lightSensor") {
            $("#lightSensor").text(res[1]);
        }

        if (res[0] == "motorControllWS") {
            $("#direction").text(res[1]);
        }
        if (res[0] == "rfidSensor") {
            $("#rfidSensor").text(res[1]);
        }
        if (res[0] == "laser") {
            $("#laser").text(res[1]);
        }

        if (res[0] == "robotArm") {
            $("#armMotors1").text(res[1]);
            $("#armMotors2").text(res[2]);
            $("#armMotors3").text(res[3]);
            $("#armMotors4").text(res[4]);
            $("#armMotors5").text(res[5]);
            $("#armMotors6").text(res[6]);
          }
        };
        websocket.onerror = function (error) {
            console.log(error);
            $("#connected").attr("class", "alert alert-danger");
            $("#connected").text("Disconnect");
          };
         websocket.onclose = function () {
             $("#connected").attr("class", "alert alert-danger");
             $("#connected").text("Disconnect");
         };
    } catch (err) {
        console.log(err);
    }
}

function sendDirection (command) {
    websocket.send(command)
}

function startAutonomerModus() {

    if (websocket != null) {
        sendDirection("autonom");
    }
    $("#startAutonomerModus").prop("disabled", true);
    $("#stopAutonomerModus").prop("disabled", false);
    $("#autonomMode").attr("class", "alert alert-danger");
    $("#autonomMode").text("Autnomer Modus AN! Don't press any Key!");
    globalCheckAutnomerModus=true;
}

function stopAutonomerModus() {

    if (websocket != null) {
        sendDirection("stoppAutonom");
    }
    $("#startAutonomerModus").prop("disabled", false);
    $("#stopAutonomerModus").prop("disabled", true);
    $("#autonomMode").attr("class", "alert alert-success");
    $("#autonomMode").text("Autonomer Modus aus!");
    globalCheckAutnomerModus=false;
}

function addCode(){
     let command = document.getElementById("command").value;
     document.getElementById("programcode").innerHTML += command+"</br>";
}

function deleteCode() {
    document.getElementById("programcode").innerHTML = "";
}

function sendCodeToRun() {
    let code = document.getElementById("programcode").innerHTML;
    sendDirection("code:"+code);
    $("#startAutonomerModus").prop("disabled", true);
    $("#stopAutonomerModus").prop("disabled", false);
    $("#autonomMode").attr("class", "alert alert-danger");
    $("#autonomMode").text("Autnomer Modus AN! Don't press any Key!");
    globalCheckAutnomerModus=true;
}

$(function () {
    document.querySelector('#disconnect').addEventListener('click', disconnect);
    document.querySelector('#connect').addEventListener('click', connect);
    document.querySelector('#startAutonomerModus').addEventListener('click', startAutonomerModus);
    document.querySelector('#stopAutonomerModus').addEventListener('click', stopAutonomerModus);
    $("#stopAutonomerModus").prop("disabled", true);
    // connect to websocket
    connect();
    // set webcam image
    imageFile = window.location.protocol + "//" + window.location.hostname + ":8080/?action=stream"
    checkImageExists(imageFile, function (existsImage) {
        if (existsImage == true) {
            $("#webcam").attr(
                "src",
                window.location.protocol + "//" + window.location.hostname
                + ":8080/?action=stream");
            $("#webcam").attr(
                "style",
                "opacity: 1.0;");
        } else {
            setInterval(slideshowPicture, 5000);
        }
    });

    // key events
    $('body').on(
        'keypress',
        function (e) {
            var x = e.which;
            var key = String.fromCharCode(x).toUpperCase();

            if (websocket != null && !globalCheckAutnomerModus) {
                sendDirection(key + ":pressed");
            }
        });

    $('body').on(
        'keyup',
        function (e) {
            var x = e.which;

            var key = String.fromCharCode(x);

            for (i = 0; i < 5; i++) {
                if (websocket != null && !globalCheckAutnomerModus) {
                    sendDirection(key + ":released");
                }
            }
        });
});
